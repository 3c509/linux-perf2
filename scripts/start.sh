#!/bin/sh

BASE="/home/alex/publish/linux-perf2"

function Usage {
  echo "Usage: $0 -h <hostname>"
}

function Work {
  $BASE/scripts/cleanup.pl
  $BASE/scripts/cp_files.pl -h $HOST
  $BASE/scripts/mkbuild.pl
  cd $BASE/build
  ./build.sh
  if [ $? -eq 0 ]; then
    echo "Completed. Charts are in reports/$HOST"
  fi
}

if [ $# -ne 2 ]; then
  Usage
  exit 1
fi

if [ $1 != "-h" ]; then
  Usage
  exit 1
fi

while getopts ":h:" opt; do
  case $opt in
    h)
      if [ ! -d $BASE/inqueue/$OPTARG ]; then
        echo "Error: unknown host $OPTARG"
        exit 1
      fi
      HOST=$OPTARG
      Work 
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done


exit 0
