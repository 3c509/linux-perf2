#!/usr/bin/perl

use File::Copy;

$BASE = "/home/alex/publish/linux-perf2";

sub Usage {
  print "Usage: $0\n";
}

# delete contents of $BASE/build
$build = $BASE . "/build";
opendir BL, $build or die "can't opendir: $build: $!";
while ($file = readdir(BL)) {
  next if ($file =~ m/^\./);
  $f = $build . "/" . $file;
  unlink $f or warn "Could not unlink $file: $!";
}
close BL;
exit 0;
