#!/usr/bin/perl
use File::Copy;

$BASE = "/home/alex/publish/linux-perf2";

sub Usage {
  print "Usage: $0\n";
}


$hstfile = $BASE . "/build/hostname\n";

open FP, $hstfile or die "can't read file $hstfile: $!";
chomp (@array = (<FP>));
close FP;
$hst = $array[0];

$outdir = $BASE . "/reports/" . $hst;
mkdir $outdir, 0755;

opendir BL, $BASE . "/build" or die "can't opendir: $BASE/build: $!";
while ($file = readdir(BL)) {
  next if ($file =~ m/^\./);
  if ($file =~ /.*png/) {
    copy($BASE ."/build/" . $file, $outdir . "/" . $file) or die "copy failed $!";
  }
}
close BL;

exit 0;
