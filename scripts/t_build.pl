#!/usr/bin/perl

use Getopt::Std;
use File::Copy;

getopts('t:d:');

sub Usage {
  print "Usage: $0 -t template -d datfile\n";
}

if (!defined $opt_t || !defined $opt_d) {
  Usage;
  exit 1;
}

if ( ! -f $opt_t ) {
  print "not a valid file : $opt_t\n";
  exit 1;
}

if ( ! -f $opt_d ) {
  print "not a valid file : $opt_d\n";
  exit 1;
}

$datfile = $opt_d;
$template = $opt_t;

open RG, $datfile or die;
chomp (@r = (<RG>));
close RG;

# find start and end for xrange
$num = scalar @r;
$num--;
$start = (split / /, $r[0])[0];
$end =  (split / /, $r[$num])[0];

open FP, $template or  die;
chomp (@array = (<FP>));
close FP;

# build output script name
$out = $template;
$out =~ s/^t_//;

open XX, '>', $out or die;
foreach $line (@array) {
  if ($line =~ /RANGE/) {
    print XX "set xrange [\"" . $start . "\":\"" . $end . "\"]" . "\n";
  } else {
    print XX  $line . "\n";
  }
  # find input filename for gnuplot script
  if ($line =~ /^plot/) {
    $input = (split / /, $line)[1];
    $input =~ s/^\"//;
    $input =~ s/\"$//;
    $input =~ s/^\'//;
    $input =~ s/\'$//;
  }
}
close XX;
chmod 0755, $out;

# copy $datfile to output filename for gnuplot script
copy($datfile, $input) or die "copy failed $!";
chmod 0755, $input;

exit 0;
