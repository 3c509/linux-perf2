#!/usr/bin/perl

use Getopt::Std;
use File::Copy;

$BASE = "/home/alex/publish/linux-perf2";

getopts('h:');

sub Usage {
  print "Usage: $0 -h hostname\n";
}

if (!defined $opt_h) {
  Usage;
  exit 1;
}

if ( ! -d $BASE . "/inqueue/" . $opt_h ) {
  print "Cannot find hostname: $opt_h\n";
  exit 1;
}

$host = $opt_h;


# delete contents of $BASE/build
$build = $BASE . "/build";
opendir BL, $build or die "can't opendir: $build: $!";
while ($file = readdir(BL)) {
  next if ($file =~ m/^\./);
  $f = $build . "/" . $file;
  unlink $f or warn "Could not unlink $file: $!";
}
close BL;


# copy files
copy($BASE . "/scripts/t_build.pl",  $BASE . "/build")  or die "copy failed $!";
chmod 0755, $BASE . "/build/t_build.pl";

copy($BASE . "/scripts/cp_rpt.pl",  $BASE . "/build")  or die "copy failed $!";
chmod 0755, $BASE . "/build/cp_rpt.pl";

$qdir = $BASE . "/inqueue/" . $host;
opendir IN, $qdir or die "can't opendir: $qdir: $!";
while ($file = readdir(IN)) {
  next if ($file =~ m/^\./);
  copy($qdir . "/" . $file,  $BASE . "/build")  or die "copy failed $!";
}
close IN;

$templ = $BASE . "/templates";
opendir TE, $templ or die "can't opendir: $templ: $!";
while ($file = readdir(TE)) {
  next if ($file =~ m/^\./);
  copy($templ . "/" . $file,  $BASE . "/build")  or die "copy failed $!";
}
close TE;

$hst = $BASE . "/build/hostname";
open HN, '>', $hst or die;
print HN $host . "\n";
close HN;

exit 0;
