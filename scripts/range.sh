#!/bin/bash
#
# utility script to capture time ranges for a specified file
#

Usage()
{
  echo "Usage: $0 filename"
}



if [ $# -ne 1 ]; then
  Usage
  exit 1
fi

if [ ! -f $1 ]; then
  echo "No such file: $1"
  echo ""
  Usage
  exit 1
fi

FILE=$1

START=`head -1 ${FILE} | awk '{print $1}'`
END=`tail -1 ${FILE} | awk '{print $1}'`

echo "set xrange [\"${START}\":\"${END}\"]" > ${FILE}_r

exit 0
