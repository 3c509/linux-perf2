#!/usr/bin/perl
use File::Copy;
use POSIX qw(ceil);
use List::Util qw( min max );

$BASE = "/home/alex/publish/linux-perf2";

$outfile = $BASE . "/build/build.sh";

$filter = $BASE . "/build/filter_list.txt";

sub Filter {
  $f = $_[0];

  # read in filter list
  if ($found) {
    # read in filter data
    open XA, $filter or die "$0: cannot access $filter: $!";
    chomp (@a = (<XA>));
    close XA;  
  
    # create array with patterns to search on
    foreach $m (@a) {
      $m =~ s/^\s+//;
      $m =~ s/\s+$//;
      next if ($m =~ /^#/);
      push @pattern, $m;
    }
  } else {
    ;;
  }

  # read in source dat file
  open XB, $BASE . "/build/" . $f or die "$0: cannot access $BASE/build/$f: $!";
  chomp (@b = (<XB>));
  close XB;  

  # find first time sample and discard data since it is a cummulative count
  # of data since the system has booted
  $timestamp = (split(/ /, $b[0]))[0];
  foreach $i (@b) {
    next if ($i =~ /^$timestamp.*/);
    push @tmp, $i;
  }
  @b = @tmp;
  undef @tmp;
     

  
  # search source dat file for matching filter patterns
  if ($found) {
    # find lines matching search pattern
    foreach $pat (@pattern) {
      @filterd = grep /$pat/, @b;
    } 
  } else {
     # set filterd array to original dat file 
     # as we didn't apply any filters
     @filterd = @b;
  }

  # make backup of original file
  copy($BASE . "/build/" . $f, $BASE . "/build/" . $f . "-orig");

  # name of original dat file
  $out = $BASE . "/build/" . $f;
  
  # aggregate data
  if ($f =~ /.*iostat_dk.dat$/) {
    $type = "dk";
    foreach $ln (@filterd) {
      chomp($ln);

      ($time, $dkdev, $tps, $kbreadps, $kbwrtps, $kbread, $kbwrite) = split(/ /, $ln);


      $kbreadps = ceil($kbreadps);
      $kbwrtps = ceil($kbwrtps);
      push @wrthroug, $kbwrtps;

      $dk_read{$time} += $kbreadps;
      $dk_write{$time} += $kbwrtps;
      $dk_kbwrit{$time} += $kbwrite;

      $totalkb += $kbwrite;

      undef $kbreadps;
      undef $kbwrtps;
      undef $kbwrite;
    }

    foreach $val (@wrthroug) {
      $sum += $val;
    }
    $elements = scalar @wrthroug;

    $avgwrite = $sum / $elements;


    print STDOUT "==== Write Statistics ====\n";
    if ($totalkb/1024 > 1000) {
      print STDOUT "Total GB written was: ", (($totalkb/1024)/1024) . "\n";
    } else {
      print STDOUT "Total MB written was: ", ($totalkb/1024) . "\n";
    }
    print STDOUT "Average throughput in KB/s was: ", $avgwrite . "\n";
    print STDOUT "Max throughput in KB/s was: ", max(@wrthroug) . "\n";

    undef $sum;
    undef $val;
    undef $elements;
    undef $avgwrite;

    foreach $keyv (sort(keys %dk_read)) {

      #
      # create new array with aggregated data
      $tmp = $keyv ." ". "dkdev" ." ". "tps" ." ". $dk_read{$keyv} ." ". $dk_write{$keyv} ." ". "kbread" ." ".  $dk_kbwrit{$keyv};
      push @io, $tmp;
      undef $tmp;
    }
  

  } elsif ($f =~ /.*iostat_dx.dat$/) {
    $type = "dx";
    foreach $ln (@filterd) {
      chomp($ln);

      ($time, $dxdev, $rrqms, $wrqms, $rps, $wps, $rsecps, $wsecps, $avgrqsz, $avgqusz, $await, $svctm, $utilpct) =  split(/ /, $ln);
      

      $rps = ceil($rps);
      $wps = ceil($wps);
      $await = ceil($await);
      $utilpct = ceil($utilpct);  
 
      $dx_read{$time} += $rps;
      $dx_write{$time} += $wps;
      $dx_resp{$time} += $await;
      $dx_util{$time} += $utilpct;

      push @writeiops, $wps;

      undef $rps;
      undef $wps;
      undef $await;
      undef $utilpct;

    }


    # Caclulate max write iops, and moving avg
    foreach $val (@writeiops) {
      $sum += $val;
    }
    $elements = scalar @writeiops;
    $avgwrite = $sum / $elements;



    foreach $keyv (sort(keys %dx_read)) {

      # create new array with aggregated data
      $tmp = $keyv ." ". "dxdev" ." ". "rrqms" ." ". "wrqms" ." ". $dx_read{$keyv} ." ".  $dx_write{$keyv} ." ". "rsecps" ." ". "wsecps" ." ". "avgrqsz" ." ".  "avgqu-sz" ." ". $dx_resp{$keyv} ." ". "svctm" ." ". $dx_util{$keyv};
      push @io, $tmp;
      undef $tmp;
    }

  } else {
    @io = @filterd;
    ;;
  }
  

  # make backup of filtered file
  copy($BASE . "/build/" . $f, $BASE . "/build/" . $f . "-filt");

  # overwrite original file with new aggregated data
  open XC, '>', $out or die "$0: cannot write to file  $out: $!";
  foreach $ln (@io) {
    chomp($ln);
    print XC $ln . "\n";
  } 
  close XC;

  undef @pattern;
  undef @filterd;
  undef $type;
  undef @io;
  undef %dx_read;
  undef %dx_write;
  undef %dx_resp;
  undef %dx_util;
  undef %dk_read;
  undef %dK_write;

}

# look for filter file
$found = 0;
if (-f $filter) {
  $found++;
}

# find filenames of source dat files in $BASE/build
$build = $BASE . "/build";
opendir BL, $build or die "can't opendir: $build: $!";
while ($file = readdir(BL)) {
  next if ($file =~ m/^\./);
  if ($file =~ /.*dk.dat$/) {
    $dkdat = $file;
  }
  if ($file =~ /.*dx.dat$/) {
    $dxdat = $file;
  }
  if ($file =~ /.*vmstat_a.dat$/) {
    $vmdat = $file;
  }
}
close BL;

# found filter file, edit files and filter on patterns in file
Filter $dkdat;
Filter $dxdat;


open FP, '>', $outfile or die;
select FP;
print "#!/bin/sh\n";
print "if [ ! -f t_build.pl ]; then\n";
print "  echo \"ERROR: cannot find t_build.pl\"\n";
print "  exit 1\n";
print "fi\n";
print "./t_build.pl -t t_iostat-dk-graph1a.sh -d " . $dkdat . "\n";
print "./t_build.pl -t t_iostat-dk-graph1.sh -d " . $dkdat . "\n";
print "./t_build.pl -t t_iostat-dx-graph1.sh -d " . $dxdat . "\n";
print "./t_build.pl -t t_iostat-dx-graph2.sh -d " . $dxdat . "\n";
print "./t_build.pl -t t_iostat-dx-graph3.sh -d " . $dxdat . "\n";
print "./t_build.pl -t t_iostat-dx-graph4.sh -d " . $dxdat . "\n";
print "./t_build.pl -t t_iostat-dx-graph5.sh -d " . $dxdat . "\n";
print "./t_build.pl -t t_vmstat-a-graph1.sh -d " . $vmdat . "\n";
print "./t_build.pl -t t_vmstat-a-graph2.sh -d " . $vmdat . "\n";
print "\n";
print "./iostat-dk-graph1a.sh\n";
print "./iostat-dk-graph1.sh\n";
print "./iostat-dx-graph1.sh\n";
print "./iostat-dx-graph2.sh\n";
print "./iostat-dx-graph3.sh\n";
print "./iostat-dx-graph4.sh\n";
print "./iostat-dx-graph5.sh\n";
print "./vmstat-a-graph1.sh\n";
print "./vmstat-a-graph2.sh\n";
print "\n";
print "./cp_rpt.pl\n";
print "\n";
print "exit 0\n";
close FP;

chmod 0755, $outfile;

exit 0;
