Linux-perf2
==============
Plots Linux performance (vxstat and iostat) data collected from perfcollect*

Linux-perf2 relies on gnuplot for for chart generation.

The goal for this utility is to build a profile of a host's system resources.
It is useful to use these charts as part of planning and design.  

* Perfcollect collects vxstat (Memory & CPU), and iostat (storage) performance statistics on Linux platforms. *


Installation
--------------

Run install script.  It should update the scripts with the appropriate
installation directory $BASE and make the scripts executable.

chmod +x install
./install

Changelog
--------------
v2.2 - rewrite
v2.3 - added chart for write IOPS, iostat-dx-graph5.png
v2.4 - updated mkbuild.pl to rollup statistics for each polling period.
       improved performance from ~3 hours to process to 7.5 minutes
     - fixed error on thoughput charts (iostat -dk)
v2.5 - removed Usage() function from mkbuild.pl
     - calculated write io statistics: avg KB/s, max KB/s, total data written
     - removed first data entry from iostat since it is a cummulative stat
v2.6 - added install script



Todo List
--------------
- add option in perfcollect to collect stats on logical volumes



Instructions
--------------

1) Use the perfcollect tool to collect performance data
2) Unarchive collected data (from perfcollect) to the inqueue directory

  $ cp done_localhost.localdomain.tgz linux-perf2/inqueue
  $ cd linux-perf2/inqueue
  $ gzip -cd done_localhost.localdomain.tgz | tar -xvf -
  
3) OPTIONAL - create a filter for devices you don't want included in the report

  copy flter_list.txt into the directory of the host data you wish to filter

  $ cp linux-perf2/inqueue/filter_list.txt ~/inqueue/localhost.localdomain

  update filter_list.txt to filter (skip) on specific drives

4) Build the charts

  $ ./start.sh -h localhost.localdomain


Usage
--------------

./start.sh -h <host>

Where <host> is a directory in ~/inqueue

Charts are copied to ~/reports/<host>



Description of Reports / Charts
--------------

** iostat-dk-graph1.png **
Title: IO Throughput
Chart: KB/s Writes, KB/s Reads, KB/s Total

** iostat-dk-graph1a.png **
Title: IO Throughput
Chart: KB/s Writes, KB/s Reads

** iostat-dx-graph1.png **
Title: IOPS VS Response Time
Chart: IOPS, Response Time (ms)

** iostat-dx-graph2.png **
Title: IOPS VS Response Time
Chart: Write IOPS (Writes/s) , Read IOPS (Reads/s), Response Time (ms)

** iostat-dx-graph3.png **
Title: Total IOPS Stacked
Chart: IOPS

** iostat-dx-graph4.png **
Title: IOPS VS Response Time VS CPU Utilization
Chart: IOPS, Response Time (ms), CPU Utilization %

** iostat-dx-graph5.png **
Title: Write IOPS
Chart: Write IOPS (Writes/s)

** vmstat-a-graph1.png **
Title: Memory Utilization
Chart: Active Memory (KB), Swap Memory (KB), Free Memory (KB)

** vmstat-a-graph2.png **
Title: CPU Utilization
Chart: Total CPU Utilization% (%User + %Sys)



Filtering (ommiting specific disks)
--------------
Prior to running start.sh copy ~/inqueue/filter_list.txt to ~/inqueue/<host>
edit ~/inqueue/<host>/filter_list.txt as appropriate

Run as normal...

./start.sh -h <host>




Workflow of linux-perf2
--------------

*(manual process)*

unarchive collected data to ~/inqueue
  cp done_localhost.localdomain.tgz ~/inqueue
  gzip -cd done_localhost.localdomain.tgz | tar -xvf -
  
*(manual process)*

copy ~/inqueue/filter_list.txt into ~/inqueue/localhost.localdomain
update filter_list.txt to filter on specific drives

*(start linux-perf2)*

./start.sh -h localhost.localdomain
  # removes files from ~build directory
  cleanup.pl

  # copies files into ~build
  cp_files.pl -h localhost.localdomain

  # if filter_lists.txt exists, parse *dat files using patterns in filter_lists.txt
  # aggregate data from samples to improve processing time
  # generate build.sh script
  mkbuild.pl

  # build charts
  build/build.sh
    # generate gnuplot scripts using t_build.pl with dat file and template as arguments
    t_build.pl

    # run generated gnu plot scripts
    ./iostat-dk-graph1a.sh
    ..

    # copy *png files to ~/reports/localhost.localdomain
    cp_rpt.pl
  
  done.
