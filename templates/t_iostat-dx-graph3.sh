#!/bin/sh

gnuplot <<_EOF_
set term png size 1024, 768 truecolor font "/usr/share/fonts/liberation/LiberationSans-Regular.ttf, 14"
set out "iostat-dx-graph3.png"
set title "Total IOPS stacked" font "/usr/share/fonts/liberation/LiberationSans-BoldItalic.ttf, 18"
set bmargin 8
set tmargin 5
set lmargin 15
set rmargin 15
set ylabel "Operations per second"
set style data histogram
set style histogram rowstacked
set style fill solid border -1
set boxwidth 0.9
set xtics in rotate by 90 font "/usr/share/fonts/liberation/LiberationSans-Regular.ttf, 10" textcolor rgbcolor "blue"
plot 'iostat-dx-graph1.dat' using 5:xtic(2) title 'r/s' with boxes \
	,'' using 6 title 'w/s' with boxes
_EOF_
#set style histogram rowstacked
#set style histogram clustered gap 0
