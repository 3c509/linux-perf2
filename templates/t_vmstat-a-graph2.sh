#!/bin/sh

gnuplot <<_EOF_
set term png size 1024, 768 truecolor font "/usr/share/fonts/liberation/LiberationSans-Regular.ttf, 14"
set out "vmstat-a-graph2.png"
set title "CPU Utilization" font "/usr/share/fonts/liberation/LiberationSans-BoldItalic.ttf, 18"
set bmargin 8
set tmargin 5
set lmargin 15
set rmargin 15
set xdata time
set timefmt "%d_%b_%H:%M:%S"
RANGE
set xlabel "Time"
set format x "%d_%b_%H:%M"
set ylabel "Percentage of Resource Utilization"
set xtics in rotate by -45 font "/usr/share/fonts/liberation/LiberationSans-Regular.ttf, 10" textcolor rgbcolor "blue"
plot "vmstat-a-graph1.dat" using 1:14+1:15 title "total CPU util" with lines
_EOF_
