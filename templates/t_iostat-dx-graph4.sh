#!/bin/sh

gnuplot <<_EOF_
set term png size 1024, 768 truecolor font "/usr/share/fonts/liberation/LiberationSans-Regular.ttf, 14"
set out "iostat-dx-graph4.png"
set title "IOPS vs response time vs CPU Utilization" font "/usr/share/fonts/liberation/LiberationSans-BoldItalic.ttf, 18"
set bmargin 8
set tmargin 5
set lmargin 15
set rmargin 15
set xdata time
set timefmt "%d_%b_%H:%M:%S"
RANGE
set xlabel "Time"
set format x "%d_%b_%H:%M"
set ylabel "Operations per second"
set xtics in rotate by -45 font "/usr/share/fonts/liberation/LiberationSans-Regular.ttf, 10" textcolor rgbcolor "blue"
plot "iostat-dx-graph1.dat" using 1:5+1:6 title "iops" with lines, \
"iostat-dx-graph1.dat" using 1:11 title "response time (ms)" with lines, \
"iostat-dx-graph1.dat" using 1:13 title "cpu utilzation%" with lines 
_EOF_
